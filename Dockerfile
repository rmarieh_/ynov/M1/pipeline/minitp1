FROM alpine:3.7

RUN apk add --no-cache nodejs nodejs-npm python py-pip && \
  pip install awscli && \
  npm install -g serverless

